void call_by_reference(int *,int *);
#include<stdio.h>
int main()
{
    int c,d;
    printf("Enter two numbers\n");
    c=input();
    
    d=input();
    printf("In main c=%d d=%d ",c,d);
    call_by_reference(&c,&d);
    return 0;
}
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
void call_by_reference(int *c,int *d)
{
    int sum;
    sum=*c+*d;
    printf("sum is %d",sum);
    
}
void call_by_reference(int *,int *);
#include<stdio.h>
int main()
{
    int a,b;
    printf("Enter two numbers\n");
    scanf("%d%d",&a,&b);
    printf("Before swap a=%d,b=%d",a,b);
    call_by_reference(&a,&b);
    return 0;
    
}
void call_by_reference(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
    printf("After swapping value a=%d,b=%d",*a,*b);
}